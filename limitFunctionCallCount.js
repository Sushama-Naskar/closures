function limitFunctionCallCount(cb, n) {
    let count=n;
    return function () {
        if (count > 0) {
            count--;
            
            return cb(count);
        } else {
            return null;
        }
    }

}


module.exports = limitFunctionCallCount;
