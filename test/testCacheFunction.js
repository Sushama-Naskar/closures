const cacheFunction = require('../cacheFunction');

function cb(key) {
    console.log(`cb is invoked for ${key}`);
    return key * 10;

}

const returnedFunction = cacheFunction(cb);


//check test
function test() {
    let testArray = [1, 2, 3, 1];
    for (let testCount = 0; testCount < testArray.length; testCount++) {
        let actualResult = returnedFunction(testArray[testCount]);
        let expectedResult = testArray[testCount] * 10;
        if (actualResult === expectedResult) {
            console.log(`Testcase ${testCount + 1} passed.`);

        } else {
            console.log(`Testcase ${testCount + 1} failed.`);
        }
    }


}

test();

