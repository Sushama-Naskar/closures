const limitFunctionCallCount = require('../limitFunctionCallCount');

let numberOfInvoke = 3;
function cb(n) {
    console.log(`This cb function is allowed to be invoked ${numberOfInvoke} times.`);
    return n;

}
const returnedFunction = limitFunctionCallCount(cb, numberOfInvoke);



//check

function test(count) {
    let testCount = 0;
    let expectedResult = numberOfInvoke;
    while (testCount < count) {
        let actualResult = returnedFunction();
        expectedResult--;
        if (actualResult === expectedResult || actualResult === null) {
            console.log(`Testcase passed.`);
        } else {
            console.log(`Testcase failed.`);
        }
        testCount++;
    }


}

test(5);
