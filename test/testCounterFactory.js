const counterFactory = require('../counterFactory');

const returnedObject = counterFactory();


//check

let expectedResult = 0;
function testIncrement(incrementCount) {
    let actualResult = 0;
    for (let count = 0; count < incrementCount; count++) {
        actualResult = returnedObject.increment();
        expectedResult++;
    }
    if (actualResult === expectedResult) {
        console.log('Increment test passed.');

    } else {
        console.log('Increment test failed.')
    }
}
function testDecrement(decrementCount) {
    let actualResult = 0;
    for (let count = 0; count < decrementCount; count++) {
        actualResult = returnedObject.decrement();
        expectedResult--;
    }
    if (actualResult === expectedResult) {
        console.log('Decrement test passed.');

    } else {
        console.log('Decrement test failed.')
    }

}
testIncrement(4);
testDecrement(5);

