function cacheFunction(cb) {
    let cacheObj = {};

    return function (key) {
        if (cacheObj[key] === undefined) {
            cacheObj[key] = cb(key);

        }
        return cacheObj[key];
    }

}
module.exports = cacheFunction;